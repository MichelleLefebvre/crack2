#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "md5.h"

const int PASS_LEN=20;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    char *hash1 = md5(guess, strlen(guess));
    // Compare the two hashes
    if (strcmp(hash, hash1) == 0)
    {
        free(hash1);
        return(1);
        
        
    }
    else
    {
        free(hash1);
        return(0);
        
    }
    // Free any malloc'd memory
    
}

// Read in a file and return the array of strings.
// Use the technique we showed in class to expand the
// array as you go.
char **readfile(char *filename)
{
    //malloc space for entire file
    //get size of file
    struct stat st;
    if(stat(filename, &st) == -1)
    {
        fprintf(stderr, "Can't get info about %s\n", filename);
        exit(1);
        
    }
    int len = st.st_size;
    
    char *file = malloc(len); //unsigned char is raw bytes , char is characters
    //read entire file into memory
    FILE *f = fopen(filename, "r");
    
    if(!f)
    {
        printf("Can't open %s for read \n", filename);
        exit(1);
    }
    fread(file, 1, len, f); //
    fclose(f);
    //replace \n with \0
    int count = 0;
    
    for(int i =0; i<len; i++)
    {
        if(file[i] == '\n')
        {
            file[i] = '\0';
            count ++;
        }
    }
    //malloc space for array of pointers
    char **line = malloc((count+1) * sizeof(char*)); //characters to pointers
    
    //fill in addresses
    int word = 0;
    line[word] = file; //the first word in the fill
    word ++;
    for (int i = 1; i<len; i++)
    {
        if (file[i] == '\0' && i+1 < len)
        {
            line[word] = &file[i+1];
            word++;
        }
    }
    line[word] = NULL;
    //return address of second array
    return line;
    free(line);
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the hash file into an array of strings
    char **dict = readfile("rockyou100.txt");

    // Read the dictionary file into an array of strings
    
    char **x = readfile("hashes.txt");
    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
    
    FILE *m;
    m = fopen("decrypt.txt", "a");
    if(!m)
    {
        printf("Can't write to file");
        exit(1);
    }
    int i = 0;
    int h = 0;
    int hashfound;
    int number = 1;
    while (x[i] != NULL)
    {
            
        while (dict[h] != NULL)
        {
            
    
            hashfound = tryguess(x[i], dict[h]);   
            if (hashfound == 1)
            {
                printf("Password %d: \n %s = %s\n ", number, dict[h], x[i]);
                fprintf(m, "Password %d: \n %s = %s\n ", number, dict[h], x[i] );
                number++;
            
            }
            
            h++;
        }
        
        i++;
        h = 0;
        
    }
    
    
    free(x[0]);
    free(x);
    free(dict[0]);
    free(dict);
    fclose(m);
}
